package ch.bbw;

public class Rectangle {private int width, height;

    public Rectangle(int width, int height){
        setWidth(width);
        setHeight(height);
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    public void draw(){
     //impletate
    }
}
