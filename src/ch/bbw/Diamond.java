package ch.bbw;

public class Diamond {
    private int size;

    public Diamond(int size){
        setSize(size);
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void draw(){
    }

}
