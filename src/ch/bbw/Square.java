package ch.bbw;

public class Square {private int size;

    public Square(int size){
        setSize(size);
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void draw(){

    }

}
